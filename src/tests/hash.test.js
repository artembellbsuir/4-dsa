import * as Utils from '../app/js/dsa/Utils'
import DSA from "../app/js/dsa/DSA"

it('hash function', () => {
    const { hashMessage } = Utils

    let message = new Uint8Array([])
    let n = 5
    expect(hashMessage(message, n)).toBe(100)

    message = new Uint8Array([1])
    expect(hashMessage(message, n)).toBe(1)

    message = new Uint8Array([1, 2])
    expect(hashMessage(message, n)).toBe(4)

    message = new Uint8Array([1, 2, 3])
    expect(hashMessage(message, n)).toBe(4)
    
    message = new Uint8Array([1, 2, 3, 4])
    expect(hashMessage(message, n)).toBe(4)

    message = new Uint8Array([42, 91, 211, 0, 121, 89])
    n = 7
    expect(hashMessage(message, n)).toBe(1)
})

it('fast exponentiation', () => {
    const { fastExponentiation } = Utils

    expect(fastExponentiation(4, 7, 33)).toBe(16)
    expect(fastExponentiation(2, 7, 33)).toBe(29)
    expect(fastExponentiation(21, 7, 33)).toBe(21)

    expect(fastExponentiation(16, 3, 33)).toBe(4)
    expect(fastExponentiation(29, 3, 33)).toBe(2)
    expect(fastExponentiation(21, 3, 33)).toBe(21)
})

it('greatest common divisor', () => {
    const { gcd } = Utils

    expect(gcd(7920, 594)).toBe(198)
    expect(gcd(941, 331)).toBe(1)
    expect(gcd(431, 817)).toBe(1)
    expect(gcd(8121, 932)).toBe(1)
    expect(gcd(4444, 338)).toBe(2)
    expect(gcd(1080, 555)).toBe(15)
    expect(gcd(6126, 822)).toBe(6)
    expect(gcd(3908, 292)).toBe(4)
    expect(gcd(7212, 848)).toBe(4)
    expect(gcd(9812, 334)).toBe(2)
})

it('prime number', () => {
    const { isPrime } = Utils

    expect(isPrime(7920)).toBeFalsy()
    expect(isPrime(9477)).toBeFalsy()
    expect(isPrime(7593)).toBeFalsy()
    expect(isPrime(8851)).toBeFalsy()
    expect(isPrime(6078)).toBeFalsy()
    expect(isPrime(6078)).toBeFalsy()
    expect(isPrime(6885)).toBeFalsy()
    
    expect(isPrime(2)).toBeTruthy()
    expect(isPrime(5323)).toBeTruthy()
    expect(isPrime(9601)).toBeTruthy()
    expect(isPrime(8527)).toBeTruthy()
    expect(isPrime(7577)).toBeTruthy()
    expect(isPrime(9973)).toBeTruthy()
})

it('is number', () => {
    const { isNumber } = Utils

    expect(isNumber('')).toBeFalsy()
    expect(isNumber('0')).toBeTruthy()
    expect(isNumber('011')).toBeTruthy()
    expect(isNumber(' -011')).toBeTruthy()
    expect(isNumber('-11 ')).toBeTruthy()
    expect(isNumber('-0')).toBeFalsy()
    expect(isNumber('-1')).toBeTruthy()
    expect(isNumber('-')).toBeFalsy()
    expect(isNumber('123')).toBeTruthy()
    expect(isNumber('1230-')).toBeFalsy()
    expect(isNumber('a2')).toBeFalsy()
})