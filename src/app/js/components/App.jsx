import { hot } from "react-hot-loader/root";
import React from "react";
import "../../styles/App.css";
import { remote } from "electron";
const fs = require("fs");

import {
	Container,
	Row,
	Col,
	Form,
	Button,
	InputGroup,
	FormControl,
	Modal
} from "react-bootstrap";

import { Input } from "./Input"
import DSA from '../dsa/DSA'
import { isNumber } from "util";

import * as Utils from '../dsa/Utils'




// let rsa
class App extends React.Component {
	constructor() {
		super();

		this.state = {
			q: "109",
			p: "35317",
			h: "2",
			g: 0,
			x: "99",
			y: "",
			k: "27",
			hash: "",

			r: 0,
			s: 0,


			w: 0,
			u1: 0,
			u2: 0,
			v: 0,

			kHasProblem: false,
			hashHasProblem: true,

			plaintext: '',
			inputFilePath: '',
			outputFilePath: '',

			readBuffer: null,
			encodedBuffer: null,
			currentMode: 0,

			show: false,
			modalText: ""
		};

		this.modes = ['Encoding mode', 'Decoding mode']
	}

	handleRead(file) {
		const fd = fs.openSync(file, 'r')
		const stats = fs.fstatSync(fd)

		let bufferSize = stats.size,
			readBuffer = Buffer.alloc(bufferSize)

		fs.readSync(fd, readBuffer, 0, bufferSize, 0)
		fs.closeSync(fd)

		console.log('read')
		console.log(readBuffer)

		this.setState({readBuffer})
		this.writePlaintextFromBuffer(readBuffer)
	}

	writePlaintextFromBuffer (buffer) {
		let offset = 0, numbersPrinted = 0, plaintext = ''
		const maxNumbersNeed = 64, chunkSize = 1
		while (offset < buffer.length && numbersPrinted < maxNumbersNeed) {
			plaintext += buffer.readUInt8(offset) + ' '
			offset += chunkSize
			numbersPrinted++
		}

		this.setState({plaintext})
	}

	signBuffer() {
		let {readBuffer, r, s} = this.state

		console.log(r, s)
		if (readBuffer) {
			const signature = Buffer.from(`_r${r}_s${s}:`)
			const len = readBuffer.length + signature.length
			const signed = Buffer.concat([readBuffer, signature], len)
			this.setState({readBuffer: signed})
			console.log('signed')
			console.log(signed)
		} else {
			console.log('empty buffer')
		}

		
		// write this signed to a file - now we have signed buffer

		
	}

	handleSave() {
		const { outputFilePath, readBuffer } = this.state;

		let writeBuffer = readBuffer

		const fd = fs.openSync(outputFilePath, 'w')
		let bufferSize = writeBuffer.length

		fs.writeSync(fd, writeBuffer, 0, bufferSize, 0)
		fs.closeSync(fd)

		console.log('write')
		console.log(writeBuffer)
	}


	calculateHash() {
		const nq = parseInt(this.state.q)
		const hash = Utils.hashMessage(this.state.readBuffer, nq)
		console.log(hash)
		this.setState({hash})
	}

	handleInputFileChoose() {
		let result = remote.dialog.showOpenDialogSync({
			properties: ["openFile"]
		});
		let file = result ? result[0] : "";

		this.handleRead(file);
		this.setState({ inputFilePath: file });

		
	}

	handleOutputFileChoose() {
		let result = remote.dialog.showOpenDialogSync({
			properties: ["openFile"]
		});
		let file = result ? result[0] : "";
		this.setState({ outputFilePath: file });
	}

	

	
	areAllValid() {
		const {p, q, h, x} = this.state
		
		const a = Utils.isValidQ(q)
		const b = Utils.isValidP(p, q)
		const c = Utils.isValidH(h, p, q)
		const d = Utils.isValidX(x, q)
		return a && b && c && d
	}

	generateY() {
		const {p, q, h, x} = this.state
		const np = parseInt(p)
		const nq = parseInt(q)
		const nh = parseInt(h)
		const nx = parseInt(x)
		const ng = Utils.fastExponentiation(nh, (np - 1) / nq, np)
		const ny = Utils.fastExponentiation(ng, nx, np)
		this.setState({y: ny.toString(), g: ng})
	}

	signFile() {
		const {p, q, k, y, x, g, hash} = this.state
		const nq = parseInt(q)

		if (Utils.isValidK(k, q)) {
			const nk = parseInt(k),
				np = parseInt(p),
				nx = parseInt(x)

			const r = Utils.fastExponentiation(g, nk, np) % nq
			const kConversed = Utils.fastExponentiation(nk, nq - 2, nq)
			console.log(`k^(-1) = ${kConversed}`)
			
			const s = (kConversed * (hash + nx * r)) % nq
		
			if (r === 0 || s === 0) {
				this.setState({kHasProblem: true})
			} else {
				console.log(`hash = ${hash}`)
				console.log('r = ', r)
				console.log('s = ', s)
				this.setState({r, s, k: nk.toString(), kHasProblem: false}, () => {
					this.signBuffer()
				})
				console.log('------------------')
			}
		}
		else {
			this.setState({kHasProblem: true})
		}
	}

	checkSignature() {
		const { r, s, p, q, g, y, readBuffer } = this.state
		
		const rStartIndex = readBuffer.lastIndexOf(`_r`)
		const sStartIndex = readBuffer.lastIndexOf(`_s`)
		const endIndex = readBuffer.lastIndexOf(`:`)

		const messageBuffer = readBuffer.subarray(0, rStartIndex)
		console.log('message')
		console.log(messageBuffer)

		const rBuffer = readBuffer.subarray(rStartIndex + 2, sStartIndex)
		const sBuffer = readBuffer.subarray(sStartIndex + 2, endIndex)
		const rFetched = rBuffer.toString()
		const sFetched = sBuffer.toString()

		console.log(rFetched)
		console.log(sFetched)

		const nq = parseInt(q),
			np = parseInt(p),
			ny = parseInt(y)

		const fetchedMessageHash = Utils.hashMessage(messageBuffer, nq)
		const w = Utils.fastExponentiation(sFetched, nq - 2, nq)
		const u1 = (fetchedMessageHash * w) % q
		const u2 = (rFetched * w) % q
		const v = (Utils.fastExponentiation(g, u1, np) * Utils.fastExponentiation(ny, u2, np)) % np % nq

		this.setState({w, u1, u2, v})
		
		console.log(`w = ${w}`)
		console.log(`u1 = ${u1}`)
		console.log(`u2 = ${u2}`)
		console.log(`v = ${v}, r = ${r}`)

		if (v === r) {
			console.log("CORRECT")
			this.setState({modalText: "Digital signature is CORRECT", show: true})
		}
		else {
			console.log("BAD")
			this.setState({modalText: "Digital signature is INCORRECT", show: true})

		}
	}

	render() {
		return (
			<Container>
				
				<Row>
					<Col>
						<Modal show={this.state.show} onHide={() => this.setState({show: false})}>
							<Modal.Header closeButton>
								<Modal.Title>{this.state.modalText}</Modal.Title>
							</Modal.Header>
						</Modal>
						<Form>
							<Form.Group controlId="inputTextArea">
								<Form.Label>
									Input text ({"<"}= 64 bytes)
								</Form.Label>
								<Form.Control
									defaultValue={this.state.plaintext}
									as="textarea"
									rows="12"
								/>
							</Form.Group>
						</Form>
					</Col>
				</Row>

				<Row>
					<Col>
						<Form>
							<Form.Group controlId="keyFiles">
								<InputGroup className="mb-3">
									<InputGroup.Prepend>
										<Button
											onClick={() =>
												this.handleInputFileChoose()
											}
											variant="outline-primary"
										>
											Choose input file
										</Button>
									</InputGroup.Prepend>
									<FormControl
										readOnly
										aria-describedby="basic-addon1"
										value={this.state.inputFilePath}
									/>
								</InputGroup>
								<InputGroup className="mb-3">
									<InputGroup.Prepend>
										<Button
											onClick={() =>
												this.handleOutputFileChoose()
											}
											variant="outline-primary"
										>Choose output file</Button>
										<Button
											onClick={() => this.handleSave()}
											disabled={
												this.state.outputFilePath == ""
											}
											variant="outline-success"
										>Save</Button>

									</InputGroup.Prepend>
									<FormControl
										readOnly
										aria-describedby="basic-addon1"
										value={this.state.outputFilePath}
									/>
								</InputGroup>
							</Form.Group>
						</Form>
					</Col>
				</Row>

				<Row>
					<Col md={6}>
						<Form>
							<Form.Group controlId="lonelyRegister2">

								<Input label="q (public)"
									value={this.state.q}
									hasProblem={!Utils.isValidQ(this.state.q)}
									handler={e => this.setState({q: e.target.value})} />
								
								<Input label={"p (public)"}
									value={this.state.p}
									hasProblem={!Utils.isValidP(this.state.p, this.state.q)}
									handler={e => this.setState({p: e.target.value})} />

								<Input label={"h"}
									value={this.state.h}
									hasProblem={!Utils.isValidH(this.state.h, this.state.p, this.state.q)}
									handler={e => this.setState({h: e.target.value})} />
								<Input label={"x (private)"}
									value={this.state.x}
									hasProblem={!Utils.isValidX(this.state.x, this.state.q)}
									handler={e => this.setState({x: e.target.value})} />

								<Form.Text className="text-muted"></Form.Text>       
								<InputGroup className="mb-3">
									<InputGroup.Prepend>
										<Button 
											onClick={() => this.generateY()}
											disabled={!this.areAllValid()}
											variant="outline-primary">Generate y (public)</Button>
									</InputGroup.Prepend>
									<FormControl
										readOnly
										aria-describedby="basic-addon1"
										value={this.state.y}
									/>
								</InputGroup>

								<InputGroup className="mb-3">
									<InputGroup.Prepend>
										<InputGroup.Text id="basic-addon4">{"g"}</InputGroup.Text>
									</InputGroup.Prepend>
									<FormControl
										readOnly
										aria-describedby="basic-addon4"
										type="text"
										placeholder=""
										defaultValue={this.state.g === 0 ? "" : this.state.g.toString()}/>
								</InputGroup>

								<Input label={"k"}
									value={this.state.k}
									hasProblem={!Utils.isValidK(this.state.k, this.state.q) || this.state.kHasProblem}
									handler={e => this.setState({k: e.target.value})} />
									
								<InputGroup className="mb-3">
									<InputGroup.Prepend>
										<Button 
											onClick={() => this.calculateHash()}
											disabled={!this.areAllValid() || !this.state.readBuffer || !Utils.isValidQ(this.state.q)}
											variant="outline-primary">Hash message</Button>
									</InputGroup.Prepend>
									<FormControl
										readOnly
										aria-describedby="basic-addon1"
										value={this.state.hash}
									/>
								</InputGroup>
							</Form.Group>
						</Form>
					</Col>
					<Col md={6}>
						<Form>
							<Form.Group controlId="123">
								<Form.Text className="text-muted"></Form.Text>     
								<InputGroup className="mb-3">
									<Button
										variant="success"
										onClick={() => this.signFile()}
										disabled={!this.areAllValid() || !Utils.isValidK(this.state.k, this.state.q)}
									>Sign file</Button>
								</InputGroup>

								<InputGroup className="mb-3">
									<InputGroup.Prepend>
										<InputGroup.Text id="basic-addon4">{"r"}</InputGroup.Text>
									</InputGroup.Prepend>
									<FormControl
										readOnly
										aria-describedby="basic-addon4"
										type="text"
										placeholder=""
										defaultValue={this.state.r === 0 ? "" : this.state.r.toString()}/>
								</InputGroup>

								<InputGroup className="mb-3">
									<InputGroup.Prepend>
										<InputGroup.Text id="basic-addon4">{"s"}</InputGroup.Text>
									</InputGroup.Prepend>
									<FormControl
										readOnly
										aria-describedby="basic-addon4"
										type="text"
										placeholder=""
										defaultValue={this.state.s === 0 ? "" : this.state.s.toString()}/>
								</InputGroup>


								<InputGroup className="mb-3">
									
									<Button
										variant="primary"
										disabled={!this.state.readBuffer}
										onClick={() => this.checkSignature()}
									>Check signature</Button>
								</InputGroup>

								<InputGroup className="mb-3">
									<InputGroup.Prepend>
										<InputGroup.Text id="basic-addon4">{"w"}</InputGroup.Text>
									</InputGroup.Prepend>
									<FormControl
										readOnly
										aria-describedby="basic-addon4"
										type="text"
										placeholder=""
										defaultValue={this.state.w === 0 ? "" : this.state.w.toString()}/>
								</InputGroup>

								<InputGroup className="mb-3">
									<InputGroup.Prepend>
										<InputGroup.Text id="basic-addon4">{"u1"}</InputGroup.Text>
									</InputGroup.Prepend>
									<FormControl
										readOnly
										aria-describedby="basic-addon4"
										type="text"
										placeholder=""
										defaultValue={this.state.u1 === 0 ? "" : this.state.u1.toString()}/>
								</InputGroup>

								<InputGroup className="mb-3">
									<InputGroup.Prepend>
										<InputGroup.Text id="basic-addon4">{"u2"}</InputGroup.Text>
									</InputGroup.Prepend>
									<FormControl
										readOnly
										aria-describedby="basic-addon4"
										type="text"
										placeholder=""
										defaultValue={this.state.u2 === 0 ? "" : this.state.u2.toString()}/>
								</InputGroup>

								<InputGroup className="mb-3">
									<InputGroup.Prepend>
										<InputGroup.Text id="basic-addon4">{"v"}</InputGroup.Text>
									</InputGroup.Prepend>
									<FormControl
										readOnly
										aria-describedby="basic-addon4"
										type="text"
										placeholder=""
										defaultValue={this.state.v === 0 ? "" : this.state.v.toString()}/>
								</InputGroup>
							</Form.Group>
						</Form>
					</Col>
				</Row>

			</Container>
		);
	}
}

export default hot(App);
