import React from 'react'
import { Form, InputGroup, FormControl } from 'react-bootstrap' 

export const Input = ({ label, value, handler, hasProblem}) => {
    return (
        <>
            <Form.Text className="text-muted">{hasProblem ? "Invalid value!" : ""}</Form.Text>            
            <InputGroup className="mb-3">
                <InputGroup.Prepend>
                    <InputGroup.Text id="basic-addon3">{label}</InputGroup.Text>
                </InputGroup.Prepend>
                <FormControl
                    aria-describedby="basic-addon3"
                    type="text"
                    value={value}
                    onChange={handler}/>
            </InputGroup>
        </>
    )
} 