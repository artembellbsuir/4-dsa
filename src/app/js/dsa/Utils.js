const hashByte = (prev, byte, n) => {
    return (prev + byte) ** 2 % n
} 

export const gcd = (a, b) => {
    return b === 0 ? a : gcd(b, a % b)
}

const getRandomRange = (min, max) => {
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

export const isPrime = number => {
    if (number == 2) {
        return true
    }

    for (let i = 0; i < 100; i++) {
        const a = getRandomRange(3, number - 1)
        if (gcd(a, number) != 1)
            return false;
        if (fastExponentiation(a, number - 1, number) !== 1) {
            return false
        }
    }

    return true
}



export const fastExponentiation = (base, power, mod) => {
    //console.log(base, power, mod)
    let number = base,
        exp = power,
        x = 1

    while (exp !== 0) {
        while (exp % 2 === 0) {
            exp = Math.floor(exp / 2)
            number = (number * number) % mod
        }

        exp--
        x = (x * number) % mod
    }

    return x
}

export const hashMessage = (message, n) => {
    const h0 = 100
    let hash = h0

    message.forEach(byte => {
        hash = hashByte(hash, byte, n)        
    });

    return hash
}

export const isNumber = str => {
    const numbers = '0123456789'

    if (str === '') return false
    const s = str.trim()

    let startIndex
    if (s[0] === '-') {
        startIndex = 1
        if (s.length === 1) return false
        else if (s.length == 2 && s[1] === '0') return false 
    } else {
        startIndex = 0
    }

    for (let i = startIndex; i < s.length; i++) {
        if (numbers.indexOf(s[i]) === -1) {
            return false
        }
    }
    return true
}

export const isValidQ = q => {
    if (isNumber(q)) {
        const nq = parseInt(q)
        return (nq > 0) && isPrime(nq)
    }
    return false
}

export const isValidP = (p, q) => {
    if (isValidQ(q)) {
        if (isNumber(p)) {
            const np = parseInt(p),
                nq = parseInt(q)
            if (np > 0 && nq > 0) {
                return (np - 1) % nq === 0
            }
            return false
        }
        return false
    }
    return false
}

export const isValidH = (h, p, q) => {
    if (isValidP(p, q)) {
        if (isNumber(h)) {
            const nh = parseInt(h), 
                np = parseInt(p),
                nq = parseInt(q)
    
            if (nh > 1 && nh < (np - 1)) {
                const g = fastExponentiation(nh, (np - 1) / nq, np)        
                if (g > 1) {
                    return true
                }
                return false
            }
            return false
        }
        return false
    }
    return false
}

export const isValidX = (x, q) => {
    if (isValidQ(q)) {
        if (isNumber(x)) {
            const nx = parseInt(x),
                nq = parseInt(q)
    
            if (nx > 0 && nx < nq) {
                return true
            }
            return false
        }
        return false
    }
    return false
}


export const isValidK = (k, q) => {
    if (isValidQ(q)) {
        if (isNumber(k)) {
            const nk = parseInt(k),
                nq = parseInt(q)
    
            if (nk > 0 && nk < (nq - 1)) {
                return true
            }
            return false
        }
        return false
    }
    return false
}