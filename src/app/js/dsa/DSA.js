import { isNumber, fastExponentiation } from "./Utils"

class DSA {
    constructor() {
        this.hash = 0
        this.y = 0
    }
}

export default new DSA()