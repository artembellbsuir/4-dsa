import * as utils from './Utils'
import bigInt from 'big-integer'

class RSA {
    constructor (p, q, d) {
        this.p = p
        this.q = q
        this.d = d

        this.r = this.p *this.q
        this.eulersValue = utils.eilersFunction(this.p, this.q)

        const ext = utils.extendedEuclid(this.eulersValue, this.d) 
        this.e = ext < 0 ? ext + this.eulersValue : ext
    }

    encodeByte (byte) {
        const c = utils.fastExponentiation(byte, this.e, this.r)
        return c 
    }

    decodeByte (byte, r, d) {
        const c = utils.fastExponentiation(byte, d, r)
        return c 
    }

    get eulers () {
        return this.eulersValue
    }

    get multiplication () {
        return this.r
    }

    get publicKey () {
        return this.e
    }    
}

export default RSA