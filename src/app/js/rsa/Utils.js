import bigInt from 'big-integer'

export const isPrime = number => {
    if (number == 2) {
        return true
    }

    for (let i = 0; i < 100; i++) {
        const a = getRandomRange(3, number - 1)
        if (gcd(a, number) != 1)
            return false;
        if (fastExponentiation(a, number - 1, number) !== 1) {
            return false
        }
    }

    return true
}

export const getRandomRange = (min, max) => {
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

export const gcd = (a, b) => {
    return b === 0 ? a : gcd(b, a % b)
}

export const eilersFunction = (p, q) => {
    return (p - 1) * (q - 1)
}

export const extendedEuclid = (a, b) => {
    let d0 = a,
        d1 = b,
        x0 = 1,
        x1 = 0,
        y0 = 0,
        y1 = 1

    while (d1 > 1) {
        let q = Math.floor(d0 / d1),
            d2 = d0 % d1,
            x2 = x0 - q * x1,
            y2 = y0 - q * y1

        d0 = d1
        d1 = d2
        x0 = x1
        x1 = x2
        y0 = y1
        y1 = y2
    }

    return y1
}

export const extendedGcd = (a, b) => {
    if (a === 0) {
        let x = 0,
            y = 1
        return [x, y, b]
    }

    const [x1, y1, d] = extendedGcd(b % a, a),
        x = y1 - Math.floor(b / a) * x1,
        y = x1

    return [x, y, d]
}

export const fastExponentiation = (base, power, mod) => {
    let number = base,
        exp = power,
        x = 1

    while (exp !== 0) {
        while (exp % 2 === 0) {
            exp = Math.floor(exp / 2)
            number = (number * number) % mod
        }

        exp--
        x = (x * number) % mod
    }

    return x
}


// validations


const isNumber = str => {
    const numbers = '0123456789'

    if (str === '0') {
        return false
    }
    for (let i = 0; i < str.length; i++) {
        if (numbers.indexOf(str[i]) === -1) {
            return false
        }
    }
    return true
}

export const validatePQ = (p, q) => {
    if (p.length === 0 || q.length === 0) {
        return false
    }
    if (!isNumber(p) || !isNumber(q)) {
        return false
    }

    return isPrime(parseInt(p)) && isPrime(parseInt(q)) && 
        ((parseInt(p) * parseInt(q)) > 255) && ((parseInt(p) * parseInt(q)) < 65536)
}

export const validatePrivateKey = (key, p, q) => {
    if (!validatePQ(p, q)) {
        return false
    }
    if (key.length === 0) {
        return false
    }
    if (!isNumber(key)) {
        return false
    }

    const eilers = eilersFunction(parseInt(p), parseInt(q))
    const s = (1 < parseInt(key)) && (parseInt(key) < eilers)
    const g = gcd(parseInt(key), eilers) === 1
    return s && g
}

export const validate = (r, d) => {
    if (r.length === 0 || d.length === 0) {
        return false
    }
    if (!isNumber(r) || !isNumber(d)) {
        return false
    }

    return (parseInt(r) > 255) && (parseInt(r) < 65536)
}

const byteSize = 8
export const getChunkSize = r => {
    // return Math.ceil(Math.log2(r) / byteSize)
    return 2
}